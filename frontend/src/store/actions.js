import axios from "axios";

export const GET_MESSAGES = "GET_MESSAGES";
export const GET_MESSAGES_DATE = "GET_MESSAGES_DATE";

export const getMessages = (messages) => ({type: GET_MESSAGES, payload: messages});
export const getMessageDate = (messages) => ({type: GET_MESSAGES_DATE, payload: messages});

export const getAllMessages = (lastDate) => async (dispatch) => {
    try {
        let response;
        if(!lastDate) {
            response = await axios.get('http://localhost:8000/messages');
            dispatch(getMessages(response.data));
        } else {
            response = await axios.get(`http://localhost:8000/messages?datetime=${lastDate}`);
            dispatch(getMessageDate(response.data));
        }
    } catch (e) {
        console.log(e);
    }
}

export const postMessage = (message) => async () => {
    try {
        await axios.post('http://localhost:8000/messages', message);
    } catch (e) {
        console.log(e);
    }
}

