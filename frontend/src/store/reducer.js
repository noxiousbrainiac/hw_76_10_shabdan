import {GET_MESSAGES, GET_MESSAGES_DATE} from "./actions";

const initialState = {
    messages: [],
    messageByDates: ""
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_MESSAGES:
            return {
                ...state,
                messages: action.payload,
                messageByDates: action.payload[action.payload.length - 1].dateTime
            }
        case GET_MESSAGES_DATE:
            return {
                ...state,
                messages: [...state.messages, ...action.payload],
                messageByDates: action.payload.length === 0 ? state.messageByDates :
                    action.payload[action.payload.length - 1].dateTime
            };
        default:
            return state;
    }
}

export default reducer;