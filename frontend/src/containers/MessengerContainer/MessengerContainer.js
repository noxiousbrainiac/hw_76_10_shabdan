import React, {useEffect, useState} from 'react';
import {Container} from "@material-ui/core";
import Form from "../../components/Form/Form";
import MessagesList from "../../components/MessagesList/MessagesList";
import {useDispatch, useSelector} from "react-redux";
import {getAllMessages, postMessage} from "../../store/actions";

const MessengerContainer = () => {
    const dispatch = useDispatch();

    const [input, setInput] = useState({
        message: "",
        author: ""
    });

    const {messages, messageByDates} = useSelector(state => state);

    const inputHandle = e => {
        const {name, value} = e.target;
        setInput(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const sendMessage = e => {
        e.preventDefault();
        if (input.message.length !== 0 && input.author !== 0) {
            dispatch(postMessage(input));
            dispatch(getAllMessages(messageByDates));
        }
    }

    useEffect(() => {
        const interval = setInterval(() => dispatch(getAllMessages(messageByDates)), 2000);
        return () => clearInterval(interval);
    }, [dispatch, messageByDates]);

    return (
        <Container>
            <Form
                onchange={inputHandle}
                message={input.message}
                author={input.author}
                send={sendMessage}
            />
            <MessagesList messages={messages}/>
        </Container>
    );
};

export default MessengerContainer;