import React from 'react';
import {Card, makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    card: {
        marginBottom: "10px",
        background: "beige",
        padding: "10px"
    }
})

const MessagesList = ({messages}) => {
    const classes = useStyles();

    return (
        <div>
            {messages && messages.map(message => (
                <Card key={message.id} className={classes.card}>
                    <p><b>Author:</b> {message.author}, <b>Datetime:</b> {message.dateTime}</p>
                    <p><b>Message:</b> {message.message}</p>
                </Card>
            ))}
        </div>
    );
};

export default MessagesList;