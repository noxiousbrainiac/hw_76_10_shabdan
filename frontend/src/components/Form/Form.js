import React from 'react';
import {Button, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles({
    inputs: {
        marginBottom: "10px"
    },
    formContainer: {
        display: "flex",
        flexDirection: "column",
        width: "500px"
    },
    form: {
        marginBottom: "20px"
    }
});

const Form = ({send, onchange, message, author}) => {
    const classes = useStyles();

    return (
        <div
            className={classes.form}
        >
            <div className={classes.formContainer}>
                <TextField
                    className={classes.inputs}
                    name="author"
                    label="Author"
                    value={author}
                    onChange={event =>  onchange(event)}
                />
                <TextField
                    className={classes.inputs}
                    name="message"
                    label="Message"
                    value={message}
                    onChange={event =>  onchange(event)}
                />
            </div>
            <Button variant="contained" color="primary" onClick={e => send(e)}>
                Send
            </Button>
        </div>
    );
};

export default Form;