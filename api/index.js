const express = require('express');
const fileDb = require('./fileDb');
const cors = require('cors');
const {nanoid} = require('nanoid');

const port = 8000;
const app = express();

app.use(express.json());
app.use(cors());

app.get('/messages', ((req, res) => {
    if (!req.query.dateTime) {
        const messages = fileDb.getItems(req);
        return res.send(messages);
    }

    try {
        const messages = fileDb.getItems(req);
        res.send(messages);
    } catch (e) {
        res.status(400).send({error: "Not found"});
    }
}));

app.post('/messages',  ((req, res) => {
    if (!req.body.message || !req.body.author) {
        res.status(400).send({error: 'Data not valid'});
    } else {
        const id = nanoid();
        const dateTime = new Date().toISOString();
        const message = {...req.body, id, dateTime};
        fileDb.addItem(message);
        res.send(message);
    }
}));

app.listen(port ,() => {
    console.log("Server works on port: ", port);
});