const fs = require('fs');

const fileName = './messages/db.json';
let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getItems(req) {
        const {datetime} = req.query;
        const array = [];
        const messages = JSON.parse(fs.readFileSync(fileName));

        if (!datetime) {
            messages.forEach(item => {
                array.push(item);
            })

            array.slice(-30);
            return array;
        }

        if (datetime) {
            const messagesByDate = messages.filter(item => item.dateTime > datetime);
            messagesByDate.forEach(item => {
                array.push(item);
            });
        }

        return array;
    },
    addItem(item) {
        data.push(item);
        this.save();
    },
    save() {
        fs.writeFileSync(fileName, JSON.stringify(data));
    }
}